package com.grajkowski.demo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

	//This is very servlet like
//	@RequestMapping("home")
//	public String home(HttpServletRequest req) {
//		HttpSession session = req.getSession();
//		String name = req.getParameter("name");
//		System.out.println("hi from " + name);
//		session.setAttribute("name", name);
//		return "home";
//	}
	
	
	//Better way to do this
//	@RequestMapping("home")
//	public String home(String name, HttpSession session) {
//		System.out.println("hi " + name);
//		session.setAttribute("name", name);
//		return "home";
//	}
	
//	@RequestMapping("home")
//	public ModelAndView home(String name, HttpSession session) {
//		ModelAndView mv = new ModelAndView();
//		mv.addObject("name", name);
//		mv.setViewName("home");
//		return mv;
//	}
	
	@RequestMapping("home")
	public ModelAndView home(Unique unique) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("obj", unique);
		mv.setViewName("home");
		return mv;
	}
	
}
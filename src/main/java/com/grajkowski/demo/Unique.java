package com.grajkowski.demo;

public class Unique {

	private int uid;
	private String name;
	private double maxDmg;
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getMaxDmg() {
		return maxDmg;
	}
	public void setMaxDmg(double maxDmg) {
		this.maxDmg = maxDmg;
	}
	
	
	
}
